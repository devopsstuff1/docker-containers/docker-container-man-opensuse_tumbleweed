FROM opensuse/tumbleweed:latest

RUN sed -i 's/^rpm.install.excludedocs = yes$/# rpm.install.excludedocs = no/' /etc/zypp/zypp.conf

RUN zypper install -y man
RUN rpm -qa | grep -v ^gpg-pubkey.* | xargs zypper install --force -y
